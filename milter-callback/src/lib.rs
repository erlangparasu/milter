//! Procedural macros that generate C callback functions for use in [milter]
//! implementation.
//!
//! The attribute macros in this crate facilitate the creation of FFI callback
//! functions that are required for the configuration of a [`Milter`]. The
//! attribute macros are used to annotate ordinary Rust functions as milter
//! callbacks. A C function is then generated that delegates to the Rust
//! callback, safely, and taking care of conversion between Rust/C types.
//!
//! Callback functions serve the purpose of *event handlers* (hence the
//! nomenclature `on_*`) for the various ‘events’ that happen during an SMTP
//! conversation. For each of the stages in the milter protocol there is a
//! corresponding [attribute macro].
//!
//! # Usage
//!
//! This crate is a dependency of the [milter][crate] crate, which re-exports
//! all macros under its namespace. Thus you should not need to use this crate
//! directly.
//!
//! Note that with Rust 2018 there are two ways of importing procedural macros.
//! Nowadays, macros can be imported like other symbols:
//!
//! ```
//! use milter::{on_connect, on_close, Milter, Status};
//! ```
//!
//! That does require you to list all macros in the `use` statement, though.
//! The older syntax with `extern crate` lets you import all macros without
//! listing them explicitly:
//!
//! ```
//! #[macro_use] extern crate milter;
//!
//! use milter::{Milter, Status};
//! ```
//!
//! Both notations are acceptable.
//!
//! # Raw string inputs
//!
//! By default, callbacks receive string inputs of type `&str`, that is, UTF-8
//! strings. Where UTF-8 encoding is not desired, it is possible to substitute
//! the C string type `&CStr` for `&str` in your handler function signature in
//! order to receive the raw bytes instead.
//!
//! Contrast the following example with the one shown at [`on_header`].
//!
//! ```
//! # #[macro_use] extern crate milter_callback;
//! # use milter::{Context, Status};
//! use std::ffi::CStr;
//!
//! #[on_header(header_callback)]
//! fn handle_header(context: Context<()>, name: &CStr, value: &CStr) -> Status {
//!     //                                       ^^^^^         ^^^^^
//!     Status::Continue
//! }
//! ```
//!
//! This feature is supported wherever `&str` appears in callback function
//! arguments.
//!
//! # Callback results
//!
//! The return type of a callback function may be wrapped in a
//! [`milter::Result`] where desired. This is a convenience: as most context API
//! methods return `milter::Result`s these can then be unwrapped with the `?`
//! operator.
//!
//! Compare the following example with the one shown at [`on_eom`]. This code
//! fragment also demonstrates the use of the `?` operator enabled by choosing
//! this return type.
//!
//! ```
//! # #[macro_use] extern crate milter_callback;
//! # use milter::{Context, Status};
//! #[on_eom(eom_callback)]
//! fn handle_eom(context: Context<()>) -> milter::Result<Status> {
//!     //                                 ^^^^^^^^^^^^^^^^^^^^^^
//!     if let Some(version) = context.api.macro_value("v")? {
//!         println!("{}", version);
//!     }
//!
//!     Ok(Status::Continue)
//! }
//! ```
//!
//! This feature is supported on all callback functions.
//!
//! # Failure modes
//!
//! An `Err` result returned from a callback leads to a temporary failure
//! ([`Status::Tempfail`]) response being returned to the MTA. The milter
//! then continues to handle requests normally.
//!
//! Panicking, on the other hand, leads to immediate [shutdown] of the milter.
//! All stages switch to returning a failure response and no longer execute the
//! handler functions (however, currently executing callback handlers are
//! allowed to finish). The milter library worker processes are terminated and
//! the currently blocked invocation of `Milter::run` returns. Cleanup logic in
//! the `close` or other stages is not executed.
//!
//! The principle behind the panicking behaviour is, as elsewhere, exit as
//! quickly as possible, within the constraints posed by the milter library and
//! the FFI interface.
//!
//! The above failure modes are provided as a convenience. Use explicit error
//! handling if they don’t satisfy your requirements.
//!
//! [milter]: https://docs.rs/milter/0.2.3/milter
//! [`Milter`]: https://docs.rs/milter/0.2.3/milter/struct.Milter.html
//! [attribute macro]: #attributes
//! [crate]: https://crates.io/crates/milter
//! [`on_header`]: attr.on_header.html
//! [`milter::Result`]: https://docs.rs/milter/0.2.3/milter/type.Result.html
//! [`on_eom`]: attr.on_eom.html
//! [`Status::Tempfail`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html#variant.Tempfail
//! [shutdown]: https://docs.rs/milter/0.2.3/milter/fn.shutdown.html

mod generator;
mod tree_preds;

use crate::generator::*;
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Ident, ItemFn};

macro_rules! ident {
    ($ident:ident) => {
        ::quote::format_ident!(::std::stringify!($ident))
    };
}

macro_rules! c_type {
    ($ty:ty) => {
        ::quote::quote! { $ty }
    };
}

/// Generates a callback function of type [`NegotiateCallback`] that delegates
/// to the annotated function.
///
/// As its sole argument `on_negotiate` takes an identifier to be used as the
/// name of the generated function.
///
/// The `on_negotiate` callback is called just before the `connect` stage. It
/// enables negotiation of certain protocol features that apply to the current
/// connection. The function arguments indicate what capabilities are available.
/// The milter can then return a subset of these to signal the desired ones. The
/// signature of the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * [`Actions`] – the available actions
/// * [`ProtocolOpts`] – the available milter protocol options
///
/// Return type:
///
/// * a tuple containing the fields
///   * [`Status`] – the response status. The special response status
///     [`Status::AllOpts`] enables all available actions and protocol stages;
///     use [`Status::Continue`] to apply your own set of actions and protocol
///     options.
///   * [`Actions`] – the desired actions
///   * [`ProtocolOpts`] – the desired protocol options
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Actions, Context, ProtocolOpts, Status};
/// # struct MyData;
///
/// #[on_negotiate(negotiate_callback)]
/// fn handle_negotiate(
///     context: Context<MyData>,
///     actions: Actions,
///     protocol_opts: ProtocolOpts,
/// ) -> (Status, Actions, ProtocolOpts) {
///     (Status::AllOpts, Default::default(), Default::default())
/// }
/// ```
///
/// [`NegotiateCallback`]: https://docs.rs/milter/0.2.3/milter/type.NegotiateCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Actions`]: https://docs.rs/milter/0.2.3/milter/struct.Actions.html
/// [`ProtocolOpts`]: https://docs.rs/milter/0.2.3/milter/struct.ProtocolOpts.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
/// [`Status::AllOpts`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html#variant.AllOpts
/// [`Status::Continue`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html#variant.Continue
#[proc_macro_attribute]
pub fn on_negotiate(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(f0), c_type!(::libc::c_ulong), Binding::Actions)
        .input(ident!(f1), c_type!(::libc::c_ulong), Binding::ProtocolOpts)
        .input_unbound(ident!(f2), c_type!(::libc::c_ulong))
        .input_unbound(ident!(f3), c_type!(::libc::c_ulong))
        .input_unbound(ident!(pf0), c_type!(*mut ::libc::c_ulong))
        .input_unbound(ident!(pf1), c_type!(*mut ::libc::c_ulong))
        .input_unbound(ident!(pf2), c_type!(*mut ::libc::c_ulong))
        .input_unbound(ident!(pf3), c_type!(*mut ::libc::c_ulong))
        .ok_result_arms(quote! {
            ::std::result::Result::Ok((::milter::Status::AllOpts, _, _)) => ::milter::Status::AllOpts as ::milter::sfsistat,
            ::std::result::Result::Ok((status, actions, protocol_opts)) => {
                *pf0 = actions.bits();
                *pf1 = protocol_opts.bits();
                *pf2 = 0;
                *pf3 = 0;
                status as ::milter::sfsistat
            }
        })
        .generate()
        .into()
}

/// Generates a callback function of type [`ConnectCallback`] that delegates to
/// the annotated function.
///
/// As its sole argument `on_connect` takes an identifier to be used as the name
/// of the generated function.
///
/// The `on_connect` callback is called at the beginning of an SMTP connection.
/// The signature of the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>&amp;[str]</code> – the client’s hostname
/// * <code>[Option]&lt;[SocketAddr]&gt;</code> – the client’s internet socket
///   address, if any
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// use std::net::SocketAddr;
/// # struct MyData;
///
/// #[on_connect(connect_callback)]
/// fn handle_connect(
///     context: Context<MyData>,
///     hostname: &str,
///     socket_address: Option<SocketAddr>,
/// ) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`ConnectCallback`]: https://docs.rs/milter/0.2.3/milter/type.ConnectCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [str]: https://doc.rust-lang.org/std/primitive.str.html
/// [Option]: https://doc.rust-lang.org/std/option/enum.Option.html
/// [SocketAddr]: https://doc.rust-lang.org/std/net/enum.SocketAddr.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_connect(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(hostname), c_type!(*mut ::libc::c_char), Binding::Str)
        .input(ident!(hostaddr), c_type!(*mut ::libc::sockaddr), Binding::SocketAddr)
        .generate()
        .into()
}

/// Generates a callback function of type [`HeloCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_helo` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_helo` callback is called when a `HELO` or `EHLO` SMTP command is
/// received. The signature of the annotated function must be as specified
/// below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>&amp;[str]</code> – the client’s hostname as stated in the
///   `HELO`/`EHLO` command
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_helo(helo_callback)]
/// fn handle_helo(context: Context<MyData>, helo_host: &str) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`HeloCallback`]: https://docs.rs/milter/0.2.3/milter/type.HeloCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [str]: https://doc.rust-lang.org/std/primitive.str.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_helo(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(helohost), c_type!(*mut ::libc::c_char), Binding::Str)
        .generate()
        .into()
}

/// Generates a callback function of type [`MailCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_mail` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_mail` callback handles processing of an envelope sender (`MAIL FROM`
/// SMTP command). The signature of the annotated function must be as specified
/// below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>[Vec]&lt;&amp;[str]&gt;</code> – the SMTP command arguments, the
///   first element being the sender address
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_mail(mail_callback)]
/// fn handle_mail(context: Context<MyData>, smtp_args: Vec<&str>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`MailCallback`]: https://docs.rs/milter/0.2.3/milter/type.MailCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [Vec]: https://doc.rust-lang.org/std/vec/struct.Vec.html
/// [str]: https://doc.rust-lang.org/std/primitive.str.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_mail(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(argv), c_type!(*mut *mut ::libc::c_char), Binding::Strs)
        .generate()
        .into()
}

/// Generates a callback function of type [`RcptCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_rcpt` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_rcpt` callback handles processing of an envelope recipient (`RCPT
/// TO` SMTP command). It can be called multiple times for a single message. The
/// signature of the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>[Vec]&lt;&amp;[str]&gt;</code> – the SMTP command arguments, the
///   first element being the recipient address
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_rcpt(rcpt_callback)]
/// fn handle_rcpt(context: Context<MyData>, smtp_args: Vec<&str>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`RcptCallback`]: https://docs.rs/milter/0.2.3/milter/type.RcptCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [Vec]: https://doc.rust-lang.org/std/vec/struct.Vec.html
/// [str]: https://doc.rust-lang.org/std/primitive.str.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_rcpt(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(argv), c_type!(*mut *mut ::libc::c_char), Binding::Strs)
        .generate()
        .into()
}

/// Generates a callback function of type [`DataCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_data` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_data` callback is called at the start of the message content (before
/// header and body). The signature of the annotated function must be as
/// specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_data(data_callback)]
/// fn handle_data(context: Context<MyData>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`DataCallback`]: https://docs.rs/milter/0.2.3/milter/type.DataCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_data(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .generate()
        .into()
}

/// Generates a callback function of type [`HeaderCallback`] that delegates to
/// the annotated function.
///
/// As its sole argument `on_header` takes an identifier to be used as the name
/// of the generated function.
///
/// The `on_header` callback handles processing of a message header line. It can
/// be called multiple times for a single message. If the value spans multiple
/// lines, line breaks are represented as `\n` (a single newline), not `\r\n`.
/// The signature of the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>&amp;[str]</code> – the header name
/// * <code>&amp;[str]</code> – the header value
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_header(header_callback)]
/// fn handle_header(context: Context<MyData>, name: &str, value: &str) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`HeaderCallback`]: https://docs.rs/milter/0.2.3/milter/type.HeaderCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [str]: https://doc.rust-lang.org/std/primitive.str.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_header(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(headerf), c_type!(*mut ::libc::c_char), Binding::Str)
        .input(ident!(headerv), c_type!(*mut ::libc::c_char), Binding::Str)
        .generate()
        .into()
}

/// Generates a callback function of type [`EohCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_eoh` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_eoh` callback is called when the end of the message header is
/// reached. The signature of the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_eoh(eoh_callback)]
/// fn handle_eoh(context: Context<MyData>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`EohCallback`]: https://docs.rs/milter/0.2.3/milter/type.EohCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_eoh(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .generate()
        .into()
}

/// Generates a callback function of type [`BodyCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_body` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_body` callback handles processing of message body content. Content
/// is transferred in possibly more than one byte chunks. The callback may
/// therefore be called multiple times for a single message. The signature of
/// the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>[&amp;&lsqb;](https://doc.rust-lang.org/std/primitive.slice.html)[u8](https://doc.rust-lang.org/std/primitive.u8.html)[&rsqb;](https://doc.rust-lang.org/std/primitive.slice.html)</code>
///   – a content chunk of the message body
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_body(body_callback)]
/// fn handle_body(context: Context<MyData>, content_chunk: &[u8]) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`BodyCallback`]: https://docs.rs/milter/0.2.3/milter/type.BodyCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_body(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input_unbound(ident!(bodyp), c_type!(*mut ::libc::c_uchar))
        .input_unbound(ident!(len), c_type!(::libc::size_t))
        .extra_arg(quote! { ::std::slice::from_raw_parts(bodyp as *const u8, len) })
        .generate()
        .into()
}

/// Generates a callback function of type [`EomCallback`] that delegates to the
/// annotated function.
///
/// As its sole argument `on_eom` takes an identifier to be used as the name of
/// the generated function.
///
/// The `on_eom` callback is called when the end of the message body is reached,
/// that is the end of the SMTP `DATA` command. This is the only stage where
/// message-modifying [actions] can be taken. The signature of the annotated
/// function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_eom(eom_callback)]
/// fn handle_eom(context: Context<MyData>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`EomCallback`]: https://docs.rs/milter/0.2.3/milter/type.EomCallback.html
/// [actions]: https://docs.rs/milter/0.2.3/milter/struct.Actions.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_eom(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .generate()
        .into()
}

/// Generates a callback function of type [`AbortCallback`] that delegates to
/// the annotated function.
///
/// As its sole argument `on_abort` takes an identifier to be used as the name
/// of the generated function.
///
/// The `on_abort` callback is called when processing of the current *message*
/// is aborted by the MTA. The signature of the annotated function must be as
/// specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_abort(abort_callback)]
/// fn handle_abort(context: Context<MyData>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`AbortCallback`]: https://docs.rs/milter/0.2.3/milter/type.AbortCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_abort(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .generate()
        .into()
}

/// Generates a callback function of type [`CloseCallback`] that delegates to
/// the annotated function.
///
/// As its sole argument `on_close` takes an identifier to be used as the name
/// of the generated function.
///
/// The `on_close` callback is called when an SMTP connection is closed. It is
/// always called at the end of a connection, also when messages were aborted.
/// It may even be called as the first and only callback (without
/// `on_connect`!), for example when the MTA decides not to go ahead with this
/// connection. The signature of the annotated function must be as specified
/// below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_close(close_callback)]
/// fn handle_close(context: Context<MyData>) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`CloseCallback`]: https://docs.rs/milter/0.2.3/milter/type.CloseCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_close(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .generate()
        .into()
}

/// Generates a callback function of type [`UnknownCallback`] that delegates to
/// the annotated function.
///
/// As its sole argument `on_unknown` takes an identifier to be used as the name
/// of the generated function.
///
/// The `on_unknown` callback is called for unknown SMTP commands. The signature
/// of the annotated function must be as specified below.
///
/// Arguments:
///
/// * <code>[Context]&lt;T&gt;</code> – the callback context
/// * <code>&amp;[str]</code> – the SMTP command
///
/// Return type:
///
/// * [`Status`] – the response status
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate milter_callback;
/// use milter::{Context, Status};
/// # struct MyData;
///
/// #[on_unknown(unknown_callback)]
/// fn handle_unknown(context: Context<MyData>, smtp_cmd: &str) -> Status {
///     Status::Continue
/// }
/// ```
///
/// [`UnknownCallback`]: https://docs.rs/milter/0.2.3/milter/type.UnknownCallback.html
/// [Context]: https://docs.rs/milter/0.2.3/milter/struct.Context.html
/// [str]: https://doc.rust-lang.org/std/primitive.str.html
/// [`Status`]: https://docs.rs/milter/0.2.3/milter/enum.Status.html
#[proc_macro_attribute]
pub fn on_unknown(attr: TokenStream, item: TokenStream) -> TokenStream {
    let name = parse_macro_input!(attr as Ident);
    let handler_fn = parse_macro_input!(item as ItemFn);

    CallbackFn::new(name, handler_fn)
        .input(ident!(ctx), c_type!(*mut ::milter::SMFICTX), Binding::Context)
        .input(ident!(arg), c_type!(*const ::libc::c_char), Binding::Str)
        .generate()
        .into()
}
