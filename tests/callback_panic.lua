conn = mt.connect("inet:3335@localhost")
assert(conn, "could not open connection")

local err = mt.mailfrom(conn, "from1@example.com")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.mailfrom(conn, "from2@example.com")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_TEMPFAIL)

local err = mt.mailfrom(conn, "from3@example.com")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_TEMPFAIL)

local err = mt.disconnect(conn)
assert(err == nil, err)
